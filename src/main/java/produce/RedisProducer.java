package produce;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

import java.util.HashMap;
import java.util.Map;

public class RedisProducer {

    public static final String STREAMS_KEY = "weather_sensor:wind";

    public static void main(String[] args) {
        int numberOfMessageToSend = 1;

        if (args !=null && args.length!=0){
            numberOfMessageToSend = Integer.parseInt(args[0]);
        }

        System.out.format("\n Sending %s message(s)",numberOfMessageToSend);

        RedisClient redisClient = RedisClient.create("redis://localhost:6379");
        StatefulRedisConnection<String,String> connection = redisClient.connect();
        RedisCommands<String,String> syncCommands = connection.sync();

        for (int i=0;i<numberOfMessageToSend;i++){

            Map<String,String> messageBody = new HashMap<>();
            messageBody.put("speed","15");
            messageBody.put("direction","270");
            messageBody.put("sensor_ts",String.valueOf(System.currentTimeMillis()));
            messageBody.put("loop_info",String.valueOf(i));

            String messageId = syncCommands.xadd(
                    STREAMS_KEY,
                    messageBody
            );

            System.out.printf("\t Message %s : %s posted",messageId,messageBody);
        }

        System.out.println();
        connection.close();
        redisClient.shutdown();
    }
}
