package consume;

import io.lettuce.core.*;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;

import java.util.List;

public class RedisConsumer {

    public static final String STREAMS_KEY = "weather_sensor:wind";

    public static void main(String[] args) {
        RedisClient redisClient = RedisClient.create("redis://localhost:6379");
        StatefulRedisConnection<String,String> connection = redisClient.connect();
        RedisCommands<String,String> syncCommands = connection.sync();

        try {
            syncCommands.xgroupCreate(XReadArgs.StreamOffset.from(STREAMS_KEY,"0-0"),"application-1");
        }catch (RedisBusyException redisBusyException){
            System.out.printf("\t Group %s already exists","applications-1");
        }

        System.out.println("Waiting for messages");

        while (true){
            List<StreamMessage<String,String>> messages = syncCommands.xreadgroup(
                    Consumer.from("application-1","consumer_1"),
                    XReadArgs.StreamOffset.lastConsumed(STREAMS_KEY)
            );

            if (!messages.isEmpty()){
                for (StreamMessage<String,String> message : messages){
                    System.out.println(message);
                    syncCommands.xack(STREAMS_KEY,"application_1",message.getId());
                }
            }
        }
    }
}
